# "Clean" - simple color scheme for Vim
### Colorscheme for those who prefer minimal text highlighting

### Screenshots clean-dark | clean-light
##### gvim
![gui](https://codeberg.org/eu/vim-colors/raw/branch/res/gui.png)
##### vim in xterm
![cterm](https://codeberg.org/eu/vim-colors/raw/branch/res/cterm.png)
### Install
```sh
cp clean-dark.vim ~/.vim/colors/
cp clean-light.vim ~/.vim/colors/
```
```VimL
"Edit ~/.vimrc set:
colorscheme clean-dark
"To use GUI colors in the terminal set:
set termguicolors
"To remove highlighting of strings and numbers inside comments:
unlet c_comment_strings
```
Colorscheme clean-dark is based on [distiled](https://github.com/karoliskoncevicius/distilled-vim)
